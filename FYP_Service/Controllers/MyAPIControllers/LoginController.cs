﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class LoginController : ApiController
    {
        public ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();
        [HttpPost]
        public HttpResponseMessage CheckLoginCredentials([FromBody] Customer cust)
        {
            try
            {
                var query = (from p in se.Customers
                             where cust.Email == p.Email && p.Password == cust.Password
                             select p).SingleOrDefault() != null;

                if (query)
                {
                    var message = Request.CreateResponse(HttpStatusCode.Accepted, cust);
                    message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                    return message;
                }
                else
                {
                    var message = Request.CreateResponse(HttpStatusCode.Conflict, cust);
                    message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }


        }
    }
}
