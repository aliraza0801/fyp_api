﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class CategoryController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se =new ShehzadaElectronicsFYP1Entities1();

        public IEnumerable<Category> Get()
        {
            return se.Categories.ToList();
        }

        public HttpResponseMessage Get(int id)
        {
            var entity = se.Categories.FirstOrDefault(e => e.Category_ID == id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Category with Id " + id.ToString() + " Not Found");
            }
        }

        public HttpResponseMessage Post([FromBody] Category cat)
        {
            try
            {
                se.Categories.Add(cat);
                se.SaveChanges();

                var message = Request.CreateResponse(HttpStatusCode.Created, cat);
                message.Headers.Location = new Uri(Request.RequestUri + cat.Category_ID.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = se.Categories.FirstOrDefault(a => a.Category_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Category With Id " + id.ToString() + " Not found");
                }
                else
                {
                    se.Categories.Remove(entity);
                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody] Category cat)
        {
            try
            {
                var entity = se.Categories.FirstOrDefault(a => a.Category_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Category With Id " + id.ToString() + " Not found");
                }
                else
                {
                    entity.Category_Name = cat.Category_Name;
                    entity.Category_Description = cat.Category_Description;

                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
