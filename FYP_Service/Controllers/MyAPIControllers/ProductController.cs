﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;
using Newtonsoft.Json.Linq;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class ProductController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se =new ShehzadaElectronicsFYP1Entities1();
        public IEnumerable<Product> Get()
        {
            return se.Products.ToList();
            //var query= from p in se.Products
            //           where 
        }
        [HttpGet]
         
        public HttpResponseMessage CategorizedProducts(int id)
        {
            var query = from p in se.Products
                where p.Category_ID == id
                select p;
            if (query != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, query);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Product with Id " + id.ToString() + " Not Found");
            }
        }

        public HttpResponseMessage Get(int id)
        {
            var entity = se.Products.FirstOrDefault(e => e.Product_ID == id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Product with Id " + id.ToString() + " Not Found");
            }
        }
        // Asp.Net Web API Controller to Post data to Sql Azure DB
        public HttpResponseMessage Post([FromBody] Product prod)
        {
            
            try
            {
                se.Products.Add(prod);
                se.SaveChanges();

                var message = Request.CreateResponse(HttpStatusCode.Created, prod);
                message.Headers.Location = new Uri(Request.RequestUri + prod.Product_ID.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = se.Products.FirstOrDefault(a => a.Product_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Product With Id " + id.ToString() + " Not found");
                }
                else
                {
                    se.Products.Remove(entity);
                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody] Product prod)
        {
            try
            {
                var entity = se.Products.FirstOrDefault(a => a.Product_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Product With Id " + id.ToString() + " Not found");
                }
                else
                {
                    entity.Product_Name = prod.Product_Name;
                    entity.Color = prod.Color;
                    entity.Brand = prod.Brand;
                    entity.Total_Cash_Price = prod.Total_Cash_Price;
                    entity.Total_Installment_Price = prod.Total_Installment_Price;
                    entity.Price_Per_Installment = prod.Price_Per_Installment;
                    entity.Total_Installments = prod.Total_Installments;
                    entity.Description = prod.Description;
                    entity.Available_Stock = prod.Available_Stock;
                    entity.Product_Image = prod.Product_Image;
                    entity.Product_Available = prod.Product_Available;

                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
