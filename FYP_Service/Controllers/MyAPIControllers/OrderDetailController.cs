﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class OrderDetailController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se =new ShehzadaElectronicsFYP1Entities1();

        public IEnumerable<OrderDetail> Get()
        {
            return se.OrderDetails.ToList();
        }

        public HttpResponseMessage Get(int id)
        {
            var entity = se.OrderDetails.FirstOrDefault(e => e.Order_Detail_ID == id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Order_Detail with Id " + id.ToString() + " Not Found");
            }
        }

        public HttpResponseMessage Post([FromBody] Admin adm)
        {
            try
            {
                se.Admins.Add(adm);
                se.SaveChanges();

                var message = Request.CreateResponse(HttpStatusCode.Created, adm);
                message.Headers.Location = new Uri(Request.RequestUri + adm.AdminID.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = se.OrderDetails.FirstOrDefault(a => a.Order_Detail_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Order_Detail With Id " + id.ToString() + " Not found");
                }
                else
                {
                    se.OrderDetails.Remove(entity);
                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody] OrderDetail ord)
        {
            try
            {
                var entity = se.OrderDetails.FirstOrDefault(a => a.Order_Detail_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Order_Detail With Id " + id.ToString() + " Not found");
                }
                else
                {
                    entity.Price = ord.Price;
                    entity.Quantity = ord.Quantity;
                    entity.Total_Price = ord.Total_Price;
                    entity.Size = ord.Size;
                    entity.Color = ord.Color;
                    entity.Fulfilled = ord.Fulfilled;

                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
