﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class AdminController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se =new ShehzadaElectronicsFYP1Entities1();

        public IEnumerable<Admin> Get()
        {
            return se.Admins.ToList();
        }

        public HttpResponseMessage Get(int id)
        {
            var entity = se.Admins.FirstOrDefault(e => e.AdminID == id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Admin with Id " + id.ToString() + " Not Found");
            }
        }

        public HttpResponseMessage Post([FromBody] Admin adm)
        {
            try
            {
                se.Admins.Add(adm);
                se.SaveChanges();

                var message = Request.CreateResponse(HttpStatusCode.Created, adm);
                message.Headers.Location = new Uri(Request.RequestUri + adm.AdminID.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = se.Admins.FirstOrDefault(a => a.AdminID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Admin With Id " + id.ToString() + " Not found");
                }
                else
                {
                    se.Admins.Remove(entity);
                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody] Admin adm)
        {
            try
            {
                var entity = se.Admins.FirstOrDefault(a => a.AdminID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Admin With Id " + id.ToString() + " Not found");
                }
                else
                {
                    entity.UserName = adm.UserName;
                    entity.Password = adm.Password;
                    

                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
