﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{

    public class VerifyEmailController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();

        [HttpPost]
        public HttpResponseMessage MatchVerificationCode([FromBody] Customer cust)
        {
            // one that user sent i.e. cust.verificationcode
            // one that is present in db against that email id. 
            var entity = se.Customers.SingleOrDefault(a => a.Email == cust.Email);

            if (entity.Verification_Code == cust.Verification_Code)
            {
                var message1 = Request.CreateResponse(HttpStatusCode.OK, cust);
                message1.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                return message1;
            }
            else
            {
                var message1 = Request.CreateResponse(HttpStatusCode.Conflict, cust);
                message1.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                return message1;
            }




        }
    }
}
