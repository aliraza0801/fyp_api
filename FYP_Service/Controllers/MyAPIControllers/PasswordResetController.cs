﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class PasswordResetController : ApiController
    {
        public ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();
        [HttpPost]
        public HttpResponseMessage Post([FromBody] Customer cust)
        {
            var query = (from p in se.Customers
                         where p.Email == cust.Email
                         select p).SingleOrDefault() != null;
            if (query)
            {
                var message = Request.CreateResponse(HttpStatusCode.OK, cust);
                message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                return message;
            }
            else
            {
                var message = Request.CreateResponse(HttpStatusCode.NotFound, cust);
                message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                return message;
            }
        }
    }
}
