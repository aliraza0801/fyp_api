﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class UpdatePasswordController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();

        [HttpPut]
        public HttpResponseMessage SaveNewPassword([FromBody] Customer cust)
        {
            // one that user sent i.e. cust.verificationcode
            // one that is present in db against that email id. 
            var entity = se.Customers.SingleOrDefault(a => a.Email == cust.Email);
            if (entity == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Conflict,
                    "Customer With Id " + cust.Customer_ID.ToString() + " Not found");
            }
            else
            {
                entity.Password = cust.Password;
                entity.Confirm_Password = cust.Confirm_Password;
                se.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }






        }
    }
}
