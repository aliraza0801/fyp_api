﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class SendEmailController : ApiController
    {
        public static Random random = new Random();
        public ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();

        [HttpPost]
        public HttpResponseMessage Send([FromBody] Customer cust)
        {
            const string chars = "0123456789";
            string sixDigitCode = new string(Enumerable.Repeat(chars, 6)
                .Select(s => s[random.Next(s.Length)]).ToArray());


            MailMessage message = new MailMessage();
            SmtpClient client = new SmtpClient();
            client.Host = "smtp-mail.outlook.com";
            client.Port = 587;
            message.From = new MailAddress("aliraza0801@outlook.com");
            message.To.Add(cust.Email);
            message.Body = "Your Email verification code is   : " + sixDigitCode;
            message.Subject = "Email Verification Code";
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("aliraza0801@outlook.com", "thenewboston.com");
            client.Send(message);

            var entity = se.Customers.FirstOrDefault(a => a.Email == cust.Email);
            entity.Verification_Code = sixDigitCode;
            se.SaveChanges();

            var message1 = Request.CreateResponse(HttpStatusCode.OK, cust);
            message1.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
            return message1;


        }
    }
}
