﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FYP_Service.Models;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class OrderController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();

        public IEnumerable<Order> Get()
        {
            return se.Orders.ToList();
        }

        public HttpResponseMessage Get(int id)
        {
            var entity = se.Orders.FirstOrDefault(e => e.Order_ID == id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Order with Id " + id.ToString() + " Not Found");
            }
        }

        public HttpResponseMessage Post([FromBody] Order ord)
        {
            try
            {
                se.Orders.Add(ord);
                se.SaveChanges();

                var message = Request.CreateResponse(HttpStatusCode.Created, ord);
                message.Headers.Location = new Uri(Request.RequestUri + ord.Order_ID.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = se.Orders.FirstOrDefault(a => a.Order_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Order With Id " + id.ToString() + " Not found");
                }
                else
                {
                    se.Orders.Remove(entity);
                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody] Order ord)
        {
            try
            {
                var entity = se.Orders.FirstOrDefault(a => a.Order_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Order With Id " + id.ToString() + " Not found");
                }
                else
                {
                    entity.Order_Date = ord.Order_Date;
                    entity.Purchased_Method = ord.Purchased_Method;
                    entity.Total_Order_Price = ord.Total_Order_Price;

                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
