﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using FYP_Service.Models;
using Microsoft.Ajax.Utilities;

namespace FYP_Service.Controllers.MyAPIControllers
{
    public class CustomersController : ApiController
    {
        ShehzadaElectronicsFYP1Entities1 se = new ShehzadaElectronicsFYP1Entities1();
        public IEnumerable<Customer> Get()
        {
            return se.Customers.ToList();
        }

        #region GetAction
        public HttpResponseMessage Get(int id)
        {
            var entity = se.Customers.FirstOrDefault(e => e.Customer_ID == id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Customer with Id " + id.ToString() + " Not Found");
            }
        }

        #endregion

        public HttpResponseMessage Post([FromBody] Customer cust)
        {
            try
            {
                //if ((cust.Phone).IsNullOrWhiteSpace() || (cust.First_Name).IsNullOrWhiteSpace() || (cust.Last_Name).IsNullOrWhiteSpace() || (cust.CNIC).IsNullOrWhiteSpace() ||
                //        (cust.Password).IsNullOrWhiteSpace() || (cust.Confirm_Password).IsNullOrWhiteSpace() || (cust.City).IsNullOrWhiteSpace() ||
                //        (cust.Address).IsNullOrWhiteSpace())
                //{
                //    var message = Request.CreateResponse(HttpStatusCode.NotAcceptable, cust);
                //    message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                //    return message;
                //}
                //else if (cust.Password!=cust.Confirm_Password)
                //{
                //    var message = Request.CreateResponse(HttpStatusCode.Forbidden, cust);
                //    message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                //    return message;
                //}
                //var query = (from p in se.Customers
                //    where p.Phone == cust.Phone
                //    select p).SingleOrDefault();




                if ((from p in se.Customers
                     where p.Email == cust.Email
                     select p).SingleOrDefault() == null)
                {
                    // perfrom this check for cust.phone

                    {
                        se.Customers.Add(cust);
                        se.SaveChanges();

                        var message = Request.CreateResponse(HttpStatusCode.Created, cust);
                        message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                        return message;
                    }

                }
                else
                {
                    var message = Request.CreateResponse(HttpStatusCode.Conflict, cust);
                    message.Headers.Location = new Uri(Request.RequestUri + cust.Customer_ID.ToString());
                    return message;

                }


            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }





        #region DeleteAction
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = se.Customers.FirstOrDefault(a => a.Customer_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Customer With Id " + id.ToString() + " Not found");
                }
                else
                {
                    se.Customers.Remove(entity);
                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        #endregion

        #region PutAction
        public HttpResponseMessage Put(int id, [FromBody] Customer cust)
        {
            try
            {
                var entity = se.Customers.FirstOrDefault(a => a.Customer_ID == id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Customer With Id " + id.ToString() + " Not found");
                }
                else
                {
                    entity.First_Name = cust.First_Name;
                    entity.Last_Name = cust.Last_Name;
                    entity.CNIC = cust.CNIC;
                    entity.Phone = cust.Phone;
                    entity.City = cust.City;
                    entity.Address = cust.Address;
                    entity.Customer_Image = cust.Customer_Image;
                    entity.Open_Purchases = cust.Open_Purchases;
                    entity.Completed_Purchases = cust.Completed_Purchases;
                    entity.Email = cust.Email;
                    entity.Password = cust.Password;
                    entity.Confirm_Password = cust.Confirm_Password;

                    se.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        #endregion
    }
}
